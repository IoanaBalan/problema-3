﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Problema_3
{
    class Program
    {
        static void Main(string[] args)
        {
            int nr = int.Parse(Console.ReadLine());
            int[] numere = new int[nr];

            for (int i = 0; i < nr; i++)
                numere[i] = int.Parse(Console.ReadLine());

            int a = int.Parse(Console.ReadLine());
            int b = int.Parse(Console.ReadLine());

            int j = 0;
            int[] elem_gasite = new int[0];

            while (j < nr)
            {
                if (!(numere[j] >= a && numere[j] <= b) || (numere[j] <= a && numere[j] >= b))
                {
                    Array.Resize(ref elem_gasite, elem_gasite.Length + 1);
                    elem_gasite[elem_gasite.Length - 1] = numere[j];
                }
                j++;
            }
            Console.WriteLine("Numarul elementelor care nu sunt cuprinse intre a si b este: " + elem_gasite.Length);
            for (int i = 0; i < elem_gasite.Length; i++)
            {
                Console.Write(elem_gasite[i] + " ");
            }

            Console.ReadKey();
        }
    }
}